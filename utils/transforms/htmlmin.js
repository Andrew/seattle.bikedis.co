const htmlmin = require('html-minifier');

module.exports = (content, outputPath) => {
  if (outputPath && outputPath.endsWith('.html')) {
    const minified = htmlmin.minify(content, {
      removeComments: true,
      collapseBooleanAttributes: true,
      collapseWhitespace: true,
      minifyCSS: true,
    });
    return minified;
  }

  return content;
};
