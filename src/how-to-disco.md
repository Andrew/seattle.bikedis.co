---
layout: page
title: How To Disco /?/ Seattle Bike Disco
ogtype: website
body_class: how-to-disco
---

# WHAT’S A BIKE DISCO?
Monthly night ride and dance party. Light yourself, light your bike, and come party with us. We roll _rain or shine_.

## THE RIDE
_7:00pm_ - Meet at [Red Square](https://goo.gl/maps/vfMLu3moicE3vQNXA) on UW campus  
_7:30pm_ - Roll out  
_~11:30pm&mdash;1:00am_ - Wrap up

_Ride up to 5 miles_ - happy hour  
_Ride up to 5 miles_ - dance / supply stop  
_Ride up to 10 miles_ - party  
_(see maps of previous rides on the gram-a-lam)_

_note_: Socials may not be updated as we go, but feel free to meet up or head out along the way; but please let the party caboose know so we aren’t waiting up!


## HOW TO BIKE
1. ACQUIRE BIKE
    - Bring your bike, duh
    - If you don’t have a bike there are some excellent bike-share options
2. LIGHT YOUR BIKE
    - Don’t forget your headlight and taillight
3. HYDRATE
    - Don’t forget your water
4. HELMET
    - Protect ye olde noggin
5. DRESS FOR THE WEATHER
    - _All weather party_
    - We will potentially be out in [PNW] cold and rainy weather, after dark, dress accordingly

## HOW TO DISCO
6. PARTY LIGHT YOURSELF + YOUR BIKE
    - Cover yourself and your bike in party lights (some examples below)
        - [Fairy Lights](https://www.amazon.com/gp/product/B0756WRTWH/)
        - [LED Strip Lights](https://www.amazon.com/gp/product/B07J2G4HQJ/)
        - [Disco Bike Light](https://www.amazon.com/gp/product/B01M3TWHJU/)
        - [Light up Gloves](https://www.amazon.com/gp/product/B01MAVOBWQ/)
7. COSTUME YOURSELF + YOUR BIKE
    - Costumes are encouraged, but not required.
    - Some months may have a theme.  Feel free to dress to the theme or however you like.
8. DE-HYDRATE
    - Bring a drink and snack for stop 1, but expect a supply shop for BEvERages and snacks along the way.
9. MAKE SOME NOISE
    - Try to bring a portable FM radio with speakers.  We are working on a system to get everyone on the same playlist...


## HOW TO VOLUNTEER
10. RIDE ROLES
    - **Regular Organizers**: Ride Leader, Party Kaboose, Patch Kit
    - **Mid-ride Volunteers**: Crossing Keepers
    - **Off-road Laborers**: Map Maker, Social Sharers
11. STARTING ANNOUNCEMENTS
    - **The People**
        - _Who’s Who_: this ride’s Leader, Party Kaboose, and Patch Kit
        - _Buddies_: identify yours, don’t leave them behind
        - _Pictures_: please let us know if you want to opt-out
        - _Tune In_: shared radio station while riding, 96.9 or 89.5 FM
    - **Route Details**
        - _Stops_: (un)known bathrooms, lack of lock-up at supply shop
        - _Not a round-trip_: find friends heading to your final destination
        - Let the Party Kaboose know if you’re peeling off
    - **Rules of the Road**
        - Keep a lane open when able; strive for fun and friendly, even to the metal boxes
        - Ride Leader will ask for Crossing Keepers, please volunteer
            - Thank the Crossing Keepers who are keeping us safe!
        - Ride moves through lights as a bus/semi/train, once we start keep rolling
        - Ride will slow/stop after turns and atop hills
        - Repeat yelled messages, if you hear it again, repeat it again
            - e.g., “Ped Up”, “Ballered”, “Pole”, “Left Turn”, “Car Back”
        - Yell “Mechanical” if you need the entire group to stop
            - Pull over as far as possible when coming to a stop, allow space for others
    - **Consent and Kindness are Key**
        - We are all here by choice
        - Respect other’s space
        - Assume good intentions
        - Leave things better than they were


