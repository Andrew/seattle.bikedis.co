// Refinement of collage layout
window.addEventListener("DOMContentLoaded", () => {
  document.querySelectorAll(".collage").forEach((collage) => {
    // References
    let pad, tail;
    const cover = collage.classList.contains("cover");
    const imgs = [...collage.querySelectorAll("img")];
    const sized = imgs.map((i) => [i, ...i.parentElement.querySelectorAll("source")]);

    // Pad
    if (cover) {
      tail = imgs.slice(-3).reverse();
      pad = document.createElement("div");
      pad.style.flexGrow = 5;
      collage.appendChild(pad);
    }

    const update = () => {
      // Pad last row (pending w3c/csswg-drafts#3070)
      if (cover) {
        const { offsetTop } = tail[0];
        pad.style.visibility = tail.some((i) => i.offsetTop !== offsetTop)
          ? "visible"
          : "collapse";
      }

      // Sync `sizes` attributes with display size
      sized.forEach((sources) => {
        const sizes = `${sources[0].clientWidth}px`;
        sources.forEach((s) => s.setAttribute("sizes", sizes));
      });
    };

    update();
    window.addEventListener("resize", () =>
      window.requestAnimationFrame(update)
    );
  });
});
