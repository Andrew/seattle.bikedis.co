---
layout: page
title: About /?/ Seattle Bike Disco
ogtype: website
body_class: about
---

## Social Media
“Official” places we can be found ’round the ’net.
- [Everyday Rides](https://everydayrides.com/groups/seattle-bike-disco)
- [Facebook](https://www.facebook.com/seattlebikedisco)
- [Instagram](https://www.instagram.com/seattlebikedisco)
- [Love To Ride](https://www.lovetoride.net/washington/groups/2098)
- [Strava](https://www.strava.com/clubs/960747)
- [Twitter](https://twitter.com/SeaBikeDisco)

```
#Bicycle #Bicycles #BicycleCostume #BicycleCostumes #BicycleCostuming
#BicycleDance #BicycleDances #BicycleDancing #BicycleDanceParty #BicycleDisco
#BicycleLife #BicycleParties #BicycleParty #BicycleSeattle #Bicycling #Bike
#Bikes #Biking #BikeCostume #BikeCostumes #BikeCostuming #BikeDance #BikeDances
#BikeDancing #BikeDanceParty #BikeDisco #BikeLife #BikeParties #BikeParty
#BikeSeattle #CostumeBike #CostumeBikes #CostumeBikers #CostumeBicycle
#CostumeBicycles #CostumeBicycling #CostumeRide #CostumeRides #CostumeRiders
#CostumeRiding #Dance #Dancer #Dancers #Dancing #DanceParty #Disco #DiscoBike
#DiscoBikes #DiscoBikers #DiscoBiking #DiscoBicycle #DiscoBicycles
#DiscoBicycler #DiscoBicyclers #DiscoBicycling #DiscoRide #DiscoRides
#DiscoRiders #DiscoRiding #GroupRide #GroupRides #GroupRiders #GroupRiding
#FridayNightRide #FridayNightRides #FridayNightRider #FridayNightRiders
#FridayNightRiding #NightBike #NightBikes #NightBikers #NightBiking
#NightBikeRide #NightBikeRiders #NightBikeRiding #NightRide #NightRiders
#NightRiding #Party #PartyBike #PartyBikes #PartyBikers #PartyBiking #PartyPace
#PNWBikeLife #RideBikes #RollingDanceParty #Seattle #SeattleBike #SeattleBikes
#SeattleBikers #SeattleBikeDisco #SeattleBikeLife #SeattleBikeParty
#SeattleBiking #iwanttoridemybicycle #youwanttorideyourbicycle
```

## Current Organizers
Meeting monthly to keep the ride rolling.
- Chrono
- Kaleena
- Kathryn
- Peaches
- Salt
- Tor

## History
Coming Soon?

## Seattle Group Rides
Other local orgs that regularly lead rides.  
Not an endorsement or recommendation, merely information sharing.  
See some we missed? Let us know!
- [Asian Bike Club (ABC) [Asian-heritage]](https://www.instagram.com/asianbikeclub/)
- [Bike Works Social Rides](https://bikeworks.org/calendar/)
- [The Bikery Social Rides [LGBTQ+]](https://www.thebikery.org/socialrides)
- [Breakfast Racing Team [FTW-NB]](https://www.breakfastcycling.club/)
- [Cascade [Free Group Rides]](https://cascade.org/fgrCalendar)
- [Coffee Outside](https://www.instagram.com/coffeeoutsidesea)
- [Critical Mass](http://bit.ly/seattleCM)
- [Cyclists of Greater Seattle (COGS)](https://cyclistsofgreaterseattle.wildapricot.org/)
- [Dead Baby Bikes](http://www.deadbabybikes.org/)
- [Emerald City Bike Party](https://www.instagram.com/ecbikeparty/)
- [Friends On Bikes (FOB) [FTW-NB]](https://www.instagram.com/fob_sea/)
- [Good Weather Social Rides](https://goodweatherinseattle.com/eventz/)
- [Moxie Monday [FTW-NB]](https://www.instagram.com/moxiemonday)
- [Northstar Cycling Club [BIPOC]](https://www.northstarcycling.org/)
- [North End Social Ride](https://www.instagram.com/northendsocialride/)
- [Peace Peloton](https://www.peacepeloton.com/)
- [Point 83 (.83)](https://point83.com/)
- [Seattle Randonneurs (SIR)](https://www.seattlerando.org/)

