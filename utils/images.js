const Image = require("@11ty/eleventy-img");
const { constants: { COPYFILE_FICLONE }, copyFileSync, mkdirSync } = require("fs");
const { dirname, join, relative } = require("path");

// Caching wrapper around eleventy-img’s source generator
exports.initCachingImage = (cacheDir) => async (path, options) => {
  const { outputDir } = options;
  options.outputDir = cacheDir;

  const meta = await Image(path, options);

  Object.values(meta).forEach((sources) =>
    sources.forEach((source) => {
      const cachePath = source.outputPath;
      source.outputPath = join(outputDir, relative(cacheDir, cachePath));

      mkdirSync(dirname(source.outputPath), { recursive: true });
      copyFileSync(cachePath, source.outputPath, COPYFILE_FICLONE);
    })
  );

  return meta;
};
