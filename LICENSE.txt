Files: *
Copyright: 2022, Wm Salt Hale salt@sal.td
License: CC BY-SA 4.0, GNU AGPLv3

The documentation provided for this project is released under a Creative Commons Attribution-ShareAlike 4.0 International License (CC BY-SA 4.0) https://creativecommons.org/licenses/by-sa/4.0/. The code provided for this project is released under the GNU Affero General Public License version 3 (GNU AGPLv3) https://www.gnu.org/licenses/agpl-3.0.
