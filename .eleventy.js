// @see https://www.11ty.io/docs/config/

// Plugins.
const Image = require('@11ty/eleventy-img');
const pluginRss = require('@11ty/eleventy-plugin-rss');
const syntaxHighlight = require('@11ty/eleventy-plugin-syntaxhighlight');
const pluginSyntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");
const pluginSEO = require('eleventy-plugin-seo');

// Utilities.
const { Settings } = require('luxon');
const { DateTime } = require("luxon");
const { join } = require('path');
const { initCachingImage } = require('./utils/images');
const contentParser = require('./utils/transforms/contentParser');
const htmlMinTransform = require('./utils/transforms/htmlmin');

// Data.
const config = require('./src/_data/config.json');
// const postTypes = require('./src/_data/postTypes.json');
const seoData = require('./src/_data/site.json');
const isDevelopment = process.env.ELEVENTY_ENV === 'development';


// const fs = require("fs");
// const pluginNavigation = require("@11ty/eleventy-navigation");
// const markdownIt = require("markdown-it");
// const markdownItAnchor = require("markdown-it-anchor");
const CachingImage = initCachingImage(join(config.paths.cache, "images"));

// Global settings.
Settings.defaultZoneName = 'US/Pacific';
const imageConfig = ({ lossless = false }) => ({
  formats: lossless ? ['png'] : [...(isDevelopment ? [] : ['avif']), 'jpeg'],
  outputDir: join(config.paths.output, config.paths.images),
  urlPath: join('/', config.paths.images),
  widths: [...(isDevelopment ? [] : [300, 600]), 1200],
});

// Exports.
module.exports = function(eleventyConfig) {
  // Add universal filters.
  // @see https://www.11ty.io/docs/filters/
  eleventyConfig.addFilter("base64", (url) =>
    Buffer.from(url).toString('base64')
  );

  // Add custom tags.
  // @see https://www.11ty.io/docs/shortcodes/
  eleventyConfig.addAsyncShortcode( 'image', async (alt, path, options = {}) => {
    const { href, lazy = true, sizes = '100vw' } = options;
    const lossless = path.endsWith('.png');

    const meta = await CachingImage(path, imageConfig({ lossless }));
    const fullSize = Object.values(meta).slice(-1)[0].slice(-1)[0].url;
    const attributes = { alt, sizes };
    if (lazy) attributes.loading = 'lazy';
    const img = Image.generateHTML(meta, attributes);

    return href === false ? img : `<a href="${href ?? fullSize}">${img}</a>`;
  });

  // Add Transforms
  // @link https://www.11ty.io/docs/config/#transforms
  // Parse the page HTML content and perform some manipulation
  eleventyConfig.addTransform('contentParser', contentParser);
  if (process.env.ELEVENTY_ENV === 'production') {
    // Minify HTML when building for production
    eleventyConfig.addTransform('htmlmin', htmlMinTransform);
  }

  // Add plugins.
  // @see https://www.11ty.dev/docs/plugins/rss/
  eleventyConfig.addPlugin(pluginRss);
  // @see https://www.11ty.dev/docs/plugins/syntaxhighlight/
  eleventyConfig.addPlugin(syntaxHighlight);
  // @see https://github.com/artstorm/eleventy-plugin-seo/
  eleventyConfig.addPlugin(pluginSEO, seoData);
  // @see https://www.11ty.dev/docs/plugins/navigation/
  // eleventyConfig.addPlugin(pluginNavigation);

  // Add custom watch targets
  // @see https://www.11ty.dev/docs/config/#add-your-own-watch-targets
  eleventyConfig.addWatchTarget(config.paths.intermediate);

  // Passthrough file copy
  // @see https://www.11ty.io/docs/copy/
  eleventyConfig.addPassthroughCopy(join(config.paths.src, '.well-known/**/*'));
  eleventyConfig.addPassthroughCopy(join(config.paths.src, '*.txt'));
  eleventyConfig.addPassthroughCopy(join(config.paths.src, 'fonts'));
  eleventyConfig.addPassthroughCopy(join(config.paths.src, 'images'));
  eleventyConfig.addPassthroughCopy(join(config.paths.src, 'files'));
  eleventyConfig.addPassthroughCopy(join(config.paths.src, 'favicon.ico'));
  eleventyConfig.addPassthroughCopy({ [config.paths.intermediate]: '.' });

  // Override BrowserSync Server options
  // @link https://www.11ty.dev/docs/config/#override-browsersync-server-options
  eleventyConfig.setBrowserSyncConfig({
    notify: false,
    open: true,
    snippetOptions: {
      rule: {
        match: /<\/head>/i,
        fn: (snippet, match) => snippet + match,
      },
    },
  });

  // Disable use gitignore for avoiding ignoring of intermediate folder during watch
  // @see https://www.11ty.dev/docs/ignores/#opt-out-of-using-.gitignore
  eleventyConfig.setUseGitIgnore(false);

  // Return configuration options.
  // @see https://www.11ty.io/docs/config/
  return {
    // -----------------------------------------------------------------
    // If your site deploys to a subdirectory, change `pathPrefix`.
    // Don’t worry about leading and trailing slashes, we normalize these.

    // If you don’t have a subdirectory, use "" or "/" (they do the same thing)
    // This is only used for link URLs (it does not affect your file structure)
    // Best paired with the `url` filter: https://www.11ty.dev/docs/filters/url/

    // You can also pass this in on the command line using `--pathprefix`

    // Optional (default is shown)
    pathPrefix: "/",
    // -----------------------------------------------------------------

    // @see https://www.11ty.io/docs/config/#input-directory
    dir: {
      input: config.paths.src,
      includes: config.paths.includes,
      layouts: config.paths.layouts,
      output: config.paths.output,
    },

    // @see https://www.11ty.io/docs/config/#default-template-engine-for-markdown-files
    templateFormats: ['njk', 'md'],
    htmlTemplateEngine: 'njk',
    markdownTemplateEngine: 'njk',
  };
};
