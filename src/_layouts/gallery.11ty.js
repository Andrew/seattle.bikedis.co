const glob = require("fast-glob").sync;
const { dirname, join, parse } = require("path");

module.exports = class {
  data = () => ({
    layout: "page.njk",
  });

  async render({ content }) {
    const inputDir = dirname(this.page.inputPath);
    const paths = glob(join(inputDir, "*.{jpg,JPG,jpeg,JPEG,png,PNG,gif,GIF}")).sort();
    const imgs = await Promise.all(
      paths.map((path, index) => {
        const alt = parse(path).name.replace(/^\d+ /, "");
        return this.image(alt, path, { lazy: index !== 0, sizes: '300px' });
      })
    );

    return `${content}<div class="collage cover">${imgs.join("")}</div>`;
  }
};
