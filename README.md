# Seattle Bike Disco

Official website for the Seattle Bike Disco!

The 11ty static site generator builds and deploys to GitLab pages.

## planning notes
- Salt would like to get something basic up but needs some guidance
- Sections being considered so far:
  - how to disco
  - gallery
  - about / history / organizers
    - links to all social accounts
    - other group rides in seattle
  - merchandise
- design thoughts:
  - logo front/center/large
  - quarter’s ride calendars below, with links to various social pages
  - top menu with links to sections above
  - link to anonymous feedback form below other content on all pages

