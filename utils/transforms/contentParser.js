const jsdom = require('jsdom');

const { JSDOM } = jsdom;

module.exports = (value, outputPath) => {
  if (outputPath && outputPath.endsWith('.html')) {
    /**
     * Create the document model
     */
    const DOM = new JSDOM(value);
    const { document } = DOM.window;

    /**
     * Enable lazy loading for images in blog posts
     */
    document.querySelectorAll('article img').forEach((img) => {
      img.setAttribute('loading', 'lazy');
    });

    return DOM.serialize();
  }
  return value;
};
